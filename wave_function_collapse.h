#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "wfc_grid.h"


//TODO: REORGANIZE AND SEPARATE TYPES FROM TOP LEVEL FUNCTIONS

#ifndef WAVE_FUNCTION_COLLAPSE_H
#define WAVE_FUNCTION_COLLAPSE_H

struct wfc_state
{
    struct wfc_grid *grid;
    struct wfc_tile_proto *protos;
    size_t num_protos;
    struct wfc_trail *trail;
};


void wfc_state_init(struct wfc_state *wfc_state, struct wfc_grid *wfc_grid,
		    struct wfc_tile_proto *wfc_protos, size_t num_protos);

//void wfc_state_init_from_ini(struct wfc_state *wfc_state, char *ini_file);


//general functions, this is not ooplands
bool wave_function_collapse(struct wfc_state *wfc_state);
//rework to check all directions.
bool wfc_propagate(struct wfc_state *wfc_state,uint16_t x, uint16_t y);

bool wfc_conflict(struct wfc_grid *grid);
bool wfc_solution(struct wfc_grid *grid);

void print_state(struct wfc_state *wfc_state);


#endif
