#include <stdint.h>
#include <stdbool.h>
#include "bitvector.h"

#ifndef BITSET_H
#define BITSET_H

struct bitset
{
    //make this a struct var_assignment?
    struct bitvector domain;
    //uint8_t  id;
    //add flags, propagators etc
};


void create_bitset(struct bitset *bitset);
void destroy_bitset(struct bitset *bitset);

//domain fuctions
void bitset_remove(struct bitset *bitset, uint8_t value);
void bitset_add(struct bitset *bitset, uint8_t value);
void bitset_add_all(struct bitset *bitset, uint8_t value);
bool bitset_contains(struct bitset *bitset, uint8_t value);
uint8_t bitset_max(struct bitset *bitset);
uint8_t bitset_min(struct bitset *bitset);
int bitset_next(struct bitset *bitset, uint8_t value);
void bitset_fix(struct bitset *bitset, uint8_t value);
void bitset_restore(struct bitset *bitset, struct bitvector restore);
uint8_t bitset_size(struct bitset *bitset);

//
uint8_t *bitset_to_array(struct bitset *bitset);
//
struct bitset bitset_union(struct bitset *bs0, struct bitset *bs1);

#endif




    
