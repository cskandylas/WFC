#include <stdint.h>
#include <stdbool.h>

#ifndef BITVECTOR_H
#define BITVECTOR_H

struct bitvector
{
    //max possible number for this is 64 si it always fits
    uint8_t num_set;
    //can hold up to 64 "things"
    uint64_t bits;
    
};

//basic operations
void bitvector_create(struct bitvector *bitvector);
void bitvector_create_explicit(struct bitvector *bitvector, uint64_t bits);
bool bitvector_isset(struct bitvector *bitvector, uint8_t idx);
void bitvector_set(struct bitvector *bitvector, uint8_t idx);
void bitvector_set_all(struct bitvector *bitvector, uint8_t idx);
void bitvector_unset(struct bitvector *bitvector, uint8_t idx);
uint8_t bitvector_max(struct bitvector *bitvector);
uint8_t bitvector_min(struct bitvector *bitvector);
void bitvector_fix(struct bitvector *bitvector,uint8_t idx);
int bitvector_next_set(struct bitvector *bitvector, uint8_t idx);
uint8_t bitvector_count_set(struct bitvector *bitvector);
struct bitvector bitvector_union(struct bitvector *bf0, struct bitvector *bf1);
//utility
uint8_t *bitvector_to_array(struct bitvector *bitvector);
#endif
