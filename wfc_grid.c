#include "wfc_grid.h"

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

void wfc_cell_init(struct wfc_cell *cell, uint8_t num_opts, uint16_t x, uint16_t y)
{
    create_bitset(&cell->opt);
    
    bitset_add_all(&cell->opt, num_opts);
    
    cell->fixed=false;
    cell->x = x;
    cell->y = y;
}
void wfc_cell_destroy(struct wfc_cell *cell)
{
    destroy_bitset(&cell->opt);
}

size_t wfc_cell_entropy(struct wfc_cell *cell)
{
    return bitset_size(&cell->opt);
}

size_t wfc_cell_fix(struct wfc_cell *cell, uint8_t opt)
{
    cell->fixed=true;
    bitset_fix(&cell->opt,opt);
}

void wfc_cell_copy(struct wfc_cell *dst, struct wfc_cell *src)
{
    //bitwise copy
    dst->opt = src->opt;
    dst->fixed = src->fixed;
    dst->x = src->x;
    dst->y = src->y;
}

void wfc_tile_proto_init(struct wfc_tile_proto *proto, uint8_t id, uint8_t freq)
{
    proto->tile_id = id;
    proto->freq= freq;
    create_bitset(&proto->up_allowed);
    create_bitset(&proto->down_allowed);
    create_bitset(&proto->left_allowed);
    create_bitset(&proto->right_allowed);

}

void wfc_tile_proto_add_one_allowed(struct wfc_tile_proto *proto, enum DIRECTION dir, uint8_t opt)
{
    if(dir == UP)
    {
	    bitset_add(&proto->up_allowed,opt);   
    }
    if(dir == DOWN)
    {
	    bitset_add(&proto->down_allowed,opt);   

    }
    if(dir == LEFT)
    {
	    bitset_add(&proto->left_allowed,opt);   
    }
    if(dir == RIGHT)
    {
	    bitset_add(&proto->right_allowed,opt);   
    }
}

void wfc_tile_proto_add_allowed(struct wfc_tile_proto *proto, enum DIRECTION dir, size_t num, ...)
{

    va_list v_a;
    va_start(v_a, num);
    if(dir == UP)
    {
	for(size_t i = 0 ; i < num;i++)
	{
	    uint8_t opt = va_arg(v_a,int);
	    bitset_add(&proto->up_allowed,opt);   
	}

    }
    if(dir == DOWN)
    {
	for(size_t i = 0 ; i < num;i++)
	{
	    uint8_t opt = va_arg(v_a,int);
	    bitset_add(&proto->down_allowed,opt);   
	}
    }
    if(dir == LEFT)
    {
	for(size_t i = 0 ; i < num;i++)
	{
	    uint8_t opt = va_arg(v_a,int);
	    bitset_add(&proto->left_allowed,opt);   
	}
    }
    if(dir == RIGHT)
    {
	for(size_t i = 0 ; i < num;i++)
	{
	    uint8_t opt = va_arg(v_a,int);
	    bitset_add(&proto->right_allowed,opt);   
	}
    }
    va_end(v_a);
}

void wfc_tile_proto_destroy(struct wfc_tile_proto *proto)
{
    //destroy struct bitset up_allowed, down_allowed, left_allowed, right_allowed;
}



void wfc_grid_init(struct wfc_grid *grid, size_t width, size_t height, uint8_t num_opts)
{
    grid->width=width;
    grid->height=height;
    grid->cells=malloc(grid->height*sizeof(struct wfc_cell*));
    for(int i=0;i<grid->height;i++)
    {
	grid->cells[i]=malloc(grid->width*sizeof(struct wfc_cell));
    }

    
    for(int j=0;j<height;j++)
    {
	for(int i=0;i<width;i++)	
	{
	    wfc_cell_init(&grid->cells[i][j], num_opts,i,j);
	}
    }
}

void wfc_grid_destroy(struct wfc_grid *grid)
{
    
    for(int j=0;j<grid->height;j++)
    {
	for(int i=0;i<grid->width;i++)
	{
	    wfc_cell_destroy(&grid->cells[i][j]);
	}
    }
    
    
    for(int i=0;i<grid->height;i++)
    {
	free(grid->cells[i]);
    }
    free(grid->cells);
}


void print_entropy(struct wfc_grid *grid)
{
    printf("Entropy:\n");
    for(size_t i=0;i<grid->height;i++)
    {
	for(size_t j=0;j<grid->width;j++)
	{
	    printf("%zu ",wfc_cell_entropy(&grid->cells[j][i]));
	}
	printf("\n");
    }
}

void print_map(struct wfc_grid *grid)
{
    printf("%zu\n",grid->width);
    for(size_t i=0;i<grid->height;i++)
    {
	for(size_t j=0;j<grid->width;j++)
	{
	    if(wfc_cell_entropy(&grid->cells[j][i]) == 1)
	    {
		printf("%u ",bitset_min(&grid->cells[j][i].opt));
	    }
	    else
	    {
		printf("? ");
	    }
	}
	printf("\n");
    }
}
