#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "bitset.h"

#ifndef WFC_GRID_H
#define WFC_GRID_H

enum DIRECTION {UP, DOWN, LEFT, RIGHT};

struct wfc_cell
{
    //holding the possible options for the cell
    struct bitset opt;
    //struct bitset up_opt, down_opt, left_opt, right_opt;
    bool fixed;
    uint16_t x, y;
};

void wfc_cell_init(struct wfc_cell *cell, uint8_t num_opts, uint16_t x, uint16_t y);
size_t wfc_cell_entropy(struct wfc_cell *cell);
size_t wfc_cell_fix(struct wfc_cell *cell, uint8_t opt);
void wfc_cell_destroy(struct wfc_cell *cell);
void wfc_cell_copy(struct wfc_cell *dst, struct wfc_cell *src);

struct wfc_tile_proto
{
    uint8_t tile_id;
    uint8_t freq;
    struct bitset up_allowed, down_allowed, left_allowed, right_allowed;
};

void wfc_tile_proto_init(struct wfc_tile_proto *proto, uint8_t id, uint8_t freq);
void wfc_tile_proto_add_allowed(struct wfc_tile_proto *proto, enum DIRECTION dir, size_t num, ...);
void wfc_tile_proto_add_one_allowed(struct wfc_tile_proto *proto, enum DIRECTION dir, uint8_t opt);

void wfc_tile_proto_destroy(struct wfc_tile_proto *proto);


struct wfc_grid
{
    struct wfc_cell **cells;
    size_t width;
    size_t height;
};

void wfc_grid_init(struct wfc_grid *grid, size_t width, size_t height, uint8_t num_opts);
void wfc_grid_destroy(struct wfc_grid *grid);

//helpers
void print_entropy(struct wfc_grid *grid);
void print_map(struct wfc_grid *grid);



#endif
