

#include "wave_function_collapse.h"
//needed
#include "trail.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>


void wfc_state_init(struct wfc_state *wfc_state, struct wfc_grid *wfc_grid,
		    struct wfc_tile_proto *wfc_protos, size_t num_protos)

{
    wfc_state->grid=wfc_grid;
    wfc_state->protos = wfc_protos;
    wfc_state->num_protos = num_protos;
    wfc_state->trail = malloc(sizeof(struct wfc_trail));
    wfc_trail_init(wfc_state->trail);
}



bool wfc_filter(struct wfc_cell *cell, struct bitset *filter)
{

    bool fixpoint = true;
    size_t num_elems = bitset_size(&cell->opt);
    if(num_elems > 0)
    {
	uint8_t *elems = bitset_to_array(&cell->opt);
	
	for(int i=0;i<num_elems;i++)
	{
	    if(!bitset_contains(filter,elems[i]))
	    {
		bitset_remove(&cell->opt,elems[i]);
		fixpoint = false;
	    }
	}
	free(elems);
    }

    return fixpoint;

}
bool wfc_propagate(struct wfc_state *wfc_state,uint16_t x, uint16_t y)
{

    struct wfc_cell *cell = &wfc_state->grid->cells[x][y];
    size_t cell_size = bitset_size(&cell->opt);
    
    if(cell_size == 0)
    {
	return false;
    }
    
    uint8_t *options = bitset_to_array(&cell->opt);
    struct bitset up_allowed;
    struct bitset down_allowed;
    struct bitset left_allowed;
    struct bitset right_allowed;
    create_bitset(&up_allowed);
    create_bitset(&down_allowed);
    create_bitset(&left_allowed);
    create_bitset(&right_allowed);
    
    for(uint8_t i=0; i<cell_size;i++)
    {
	struct wfc_tile_proto *proto = &wfc_state->protos[options[i]];
	up_allowed = bitset_union(&proto->up_allowed,&up_allowed);
	down_allowed = bitset_union(&proto->down_allowed,&down_allowed);
	left_allowed = bitset_union(&proto->left_allowed,&left_allowed);
	right_allowed = bitset_union(&proto->right_allowed,&right_allowed);
    }

    free(options);
    
    if(y > 0)
    {
	
	bool fixpoint = true;
	struct wfc_cell *next = &wfc_state->grid->cells[x][y-1];
	
	if(!next->fixed)
	{

	    //before filtering mark the cell
	    wfc_mark_add_propagated(wfc_state->trail->end, next);
	    fixpoint = wfc_filter(next,&up_allowed);
	    
	    if(!fixpoint)
	    {
		wfc_propagate(wfc_state,x,y-1);
	    }
	}
	
    }
    if(y < wfc_state->grid->height-1)
    {
	bool fixpoint = true;
	struct wfc_cell *next = &wfc_state->grid->cells[x][y+1];
	
	if(!next->fixed)
	{
	    //before filtering mark the cell
	    wfc_mark_add_propagated(wfc_state->trail->end, next);
	    fixpoint = wfc_filter(next,&down_allowed);
	    if(!fixpoint)
	    {
		
		wfc_propagate(wfc_state,x,y+1);
	    }
	}

    }

    if(x > 0)
    {
	struct wfc_cell *next = &wfc_state->grid->cells[x-1][y];

	bool fixpoint = true;
	
	if(!next->fixed)
	{
	    //before filtering mark the cell
	    wfc_mark_add_propagated(wfc_state->trail->end, next);
	    fixpoint = wfc_filter(next,&left_allowed);
	    if(!fixpoint)
	    {
		wfc_propagate(wfc_state,x-1,y);	
	    }
	}
	
    }

    if( x < wfc_state->grid->width-1)
    {
	struct wfc_cell *next = &wfc_state->grid->cells[x+1][y];
	bool fixpoint = true;
	
	if(!next->fixed)
	{
	    //before filtering mark the cell
	    wfc_mark_add_propagated(wfc_state->trail->end, next);
	    fixpoint = wfc_filter(next,&right_allowed);
	    if(!fixpoint)
	    {
		wfc_propagate(wfc_state,x+1,y);
	    }
	}
	
    }

    

    return true;
}


static uint8_t collapse(struct wfc_state *wfc_state, struct wfc_cell *cell)
{
    //TODO: Take frequency into account when building the choices table.
    uint8_t *elems = bitset_to_array(&cell->opt);
    size_t num_elems = bitset_size(&cell->opt);

    size_t num_opts = 0;
    for(int i=0;i<num_elems;i++)
    {
	struct wfc_tile_proto *proto = &wfc_state->protos[elems[i]];
	num_opts+=proto->freq;
    }

    uint8_t *options = malloc(num_opts*sizeof(uint8_t));
    size_t idx=0;
    for(int i=0;i<num_elems;i++)
    {
	struct wfc_tile_proto *proto = &wfc_state->protos[elems[i]];
	for(int j=0;j<proto->freq;j++)
	{
	    options[idx]=proto->tile_id;
	    idx++;
	}
    }
    
    //decide to fix the i-th set element
    uint8_t fix_idx = rand()%num_opts;
    uint8_t fix_tid=options[fix_idx];
    free(elems);
    free(options);
    return fix_tid;
}

bool wave_function_collapse(struct wfc_state *wfc_state)
{
    
    struct wfc_grid *grid = wfc_state->grid;
    struct wfc_tile_proto *protos = wfc_state->protos;
    size_t x_min=0,y_min=0;
    size_t min_entropy = 1000;
    for(size_t i=0;i<grid->height;i++)
    {
	for(size_t j=0;j<grid->width;j++)
	{
	    if(wfc_cell_entropy(&grid->cells[j][i]) < min_entropy && !grid->cells[j][i].fixed)
	    {
		x_min=j;
		y_min=i;
		min_entropy = wfc_cell_entropy(&grid->cells[j][i]);
	    }
	}
    }


    if(min_entropy == 0)
    {
	print_entropy(grid);
	if(!wfc_trail_backtrack(wfc_state->trail, wfc_state->grid))
	{
	    return false;
	}
	return wave_function_collapse(wfc_state);
    }


    uint8_t fix_tid = collapse(wfc_state,&grid->cells[x_min][y_min]);
    
    //start add  a trail mark before fixing
    struct wfc_mark *mark = malloc(sizeof(struct wfc_mark));
    wfc_mark_init(mark, fix_tid, &grid->cells[x_min][y_min]);
    wfc_trail_add_mark(wfc_state->trail,mark);


    wfc_cell_fix(&grid->cells[x_min][y_min],fix_tid);
    wfc_propagate(wfc_state, x_min, y_min);



    if(wfc_conflict(grid))
    {
	//we have a conflict so backtrack using the trail
	
	
	if(wfc_state->trail->end)
	{
	    //print_state(wfc_state);
	    if(!wfc_trail_backtrack(wfc_state->trail, wfc_state->grid))
	    {
		print_entropy(grid);
		print_state(wfc_state);
		return false;
	    }
	    
	}
	else
	{
	    return false;
	}
    }
    else if(wfc_solution(grid))
    {
	print_map(grid);
	return true;
    }
    
    
    return wave_function_collapse(wfc_state);
    
    
}


bool wfc_conflict(struct wfc_grid *grid)
{
    for(size_t i=0;i<grid->height;i++)
    {
	for(size_t j=0;j<grid->width;j++)
	{
	    if(wfc_cell_entropy(&grid->cells[j][i])==0)
	    {
		return true;
	    }
	}
	
    }

    return false;
}

bool wfc_solution(struct wfc_grid *grid)
{
    bool solved = true;
    for(size_t i=0;i<grid->height;i++)
    {
	for(size_t j=0;j<grid->width;j++)
	{
	    if(wfc_cell_entropy(&grid->cells[j][i])==0 || !grid->cells[j][i].fixed)
	    {
		solved = false;
		break;
	    }
	    
	}
	
    }

    return solved;
}

void print_state(struct wfc_state *wfc_state)
{

    struct wfc_grid *grid = wfc_state->grid;
    
    for(size_t i=0;i<grid->height;i++)
    {
	for(size_t j=0;j<grid->width;j++)
	{
	    struct wfc_cell *cell = &grid->cells[j][i];
	    printf("[%u,%u]:%zu\n",cell->x,cell->y,wfc_cell_entropy(cell));
	    
	}
	
    }
    
}

