#define _POSIX_C_SOURCE  200809L

#include "wave_function_collapse.h"
//needed
#include "trail.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <sys/types.h>
#include <string.h>
#include "tokenize.h"


static void proto_parse_dir(char *dir_str,enum DIRECTION dir, struct wfc_tile_proto *t_p)
{
    //printf("Dir str:%s\n",dir_str);
    struct tokenizer comma_tok;
    tokenizer_init(&comma_tok,dir_str,",");
    char *tile_id_str = next_token(&comma_tok);
    while(tile_id_str)
    {
	uint8_t opt = atoi(tile_id_str);
	//printf("opt: %u\n");
	wfc_tile_proto_add_one_allowed(t_p, dir, opt);
	free(tile_id_str);
	tile_id_str = next_token(&comma_tok);
    }
    
    
}


static bool proto_init_from_line(char *line, struct wfc_tile_proto *t_p)
{
    struct tokenizer colon_tok;
    tokenizer_init(&colon_tok,line,":");
    char *tileid_str = next_token(&colon_tok);
    char *freq_str = next_token(&colon_tok);
    uint8_t tile_id=atoi(tileid_str);
    uint8_t frequency=atoi(freq_str);
    //printf("%u:%u\n",tile_id,frequency);;
    wfc_tile_proto_init(t_p, tile_id, frequency);

    char *directions = next_token(&colon_tok);
    
    struct tokenizer dir_tok;
    tokenizer_init(&dir_tok,directions," []");
    char *up = next_token(&dir_tok);
    proto_parse_dir(up,UP,t_p);
    char *down = next_token(&dir_tok);
    proto_parse_dir(down,DOWN,t_p);
    char *left = next_token(&dir_tok);
    proto_parse_dir(left,LEFT,t_p);
    char *right = next_token(&dir_tok);
    proto_parse_dir(right,RIGHT,t_p);

    free(tileid_str);
    free(freq_str);
    free(directions);
    free(up);
    free(down);
    free(left);
    free(right);
    //
    return true;
}

static void state_init_from_file(char *file, struct wfc_state *s)
{
    size_t grid_size = 0;
    size_t num_tiles = 0;

    FILE *stream;
    char *line = NULL;
    size_t len = 0;
    ssize_t nread;
    
    stream = fopen(file, "r");
    if (!stream) {
	fprintf(stderr,"Could not open: %s, panic exiting\n",file);
	exit(-1);
    }


    //read the grid size and number of tiles
    if(nread = getline(&line, &len, stream)!=-1)
    {
	char *delim = strchr(line,':');
	if(!delim)
	{
	    fprintf(stderr,"File seems to not be a valid specification for WFC\n",file);
	    exit(-1);
	}
	else
	{
	    //get grid size
	    grid_size = atoi(line);
	    //get number of tiles
	    num_tiles = atoi(delim+1);
	    //printf("Grid size: %zu\n",grid_size);
	    //printf("Tile number: %zu\n", num_tiles);
	}
	
    }
    else
    {
	
	fprintf(stderr,"Empty specification file: %s, panic exiting\n",file);
	exit(-1);
    }

    if(nread = getline(&line, &len, stream)!=1)
    {
	fprintf(stderr, "Line 1 is not empty!\n");
    }

    struct wfc_tile_proto *protos = malloc(num_tiles * sizeof(struct wfc_tile_proto));
    
    size_t protos_read = 0;
    
    while ((nread = getline(&line, &len, stream)) != -1 && protos_read < num_tiles)
    {
	//printf("Retrieved line of length %zd:\n", nread);
	if(nread > 0)
	{
	    if(proto_init_from_line(line, &protos[protos_read]))
	    {
		protos_read++;
	    }
	    

	}
	
    }
    
    free(line);
    fclose(stream);
    if(protos_read == num_tiles)
    {
	struct wfc_grid *grid = malloc(sizeof(struct wfc_grid));
	wfc_grid_init(grid,grid_size, grid_size,num_tiles);
	wfc_state_init(s,grid,protos,protos_read);
	
    }
}




int main(int argc, char *argv[])
{

    //printf("%zu:%zu\n",sizeof(struct bitfield), sizeof(struct bitset));
    int seed = time(NULL);
    srand(seed);
    struct wfc_state state;
    state_init_from_file(argv[1], &state);
    wave_function_collapse(&state);
    wfc_trail_destroy(state.trail);
    free(state.trail);
    wfc_grid_destroy(state.grid);
    free(state.grid);
    free(state.protos);
    return 0;
    
    
    
    int grid_size = 16;
    if(argc >=2)
    {
	grid_size = atoi(argv[1]);
    }
    if(argc >= 3)
    {
	seed = atoi(argv[2]);
	
    }

    

    int num_tiles = 14;
    
    struct wfc_grid grid;
    wfc_grid_init(&grid,grid_size,grid_size,num_tiles);

    struct wfc_tile_proto rules[num_tiles];

    wfc_tile_proto_init(&rules[0],0,1);
    wfc_tile_proto_add_allowed(&rules[0],UP,1,13);
    wfc_tile_proto_add_allowed(&rules[0],DOWN,2,3,12);
    wfc_tile_proto_add_allowed(&rules[0],LEFT,1,13);
    wfc_tile_proto_add_allowed(&rules[0],RIGHT,2,1,12);

    wfc_tile_proto_init(&rules[1],1,1);
    wfc_tile_proto_add_allowed(&rules[1],UP,1,13);
    wfc_tile_proto_add_allowed(&rules[1],DOWN,1,4);
    wfc_tile_proto_add_allowed(&rules[1],LEFT,2,0,1);
    wfc_tile_proto_add_allowed(&rules[1],RIGHT,3,1,2,12);

    wfc_tile_proto_init(&rules[2],2,1);
    wfc_tile_proto_add_allowed(&rules[2],UP,1,13);
    wfc_tile_proto_add_allowed(&rules[2],DOWN,1,5);
    wfc_tile_proto_add_allowed(&rules[2],LEFT,1,1);
    wfc_tile_proto_add_allowed(&rules[2],RIGHT,1,13);

    wfc_tile_proto_init(&rules[3],3,1);
    wfc_tile_proto_add_allowed(&rules[3],UP,2,0,3);
    wfc_tile_proto_add_allowed(&rules[3],DOWN,2,3,6);
    wfc_tile_proto_add_allowed(&rules[3],LEFT,1,13);
    wfc_tile_proto_add_allowed(&rules[3],RIGHT,1,4);

    wfc_tile_proto_init(&rules[4],4,1);
    wfc_tile_proto_add_allowed(&rules[4],UP,2,1,4);
    wfc_tile_proto_add_allowed(&rules[4],DOWN,2,4,7);
    wfc_tile_proto_add_allowed(&rules[4],LEFT,2,3,4);
    wfc_tile_proto_add_allowed(&rules[4],RIGHT,2,4,5);

    wfc_tile_proto_init(&rules[5],5,1);
    wfc_tile_proto_add_allowed(&rules[5],UP,2,2,5);
    wfc_tile_proto_add_allowed(&rules[5],DOWN,2,5,8);
    wfc_tile_proto_add_allowed(&rules[5],LEFT,1,4);
    wfc_tile_proto_add_allowed(&rules[5],RIGHT,1,13);

    wfc_tile_proto_init(&rules[6],6,1);
    wfc_tile_proto_add_allowed(&rules[6],UP,2,0,3);
    wfc_tile_proto_add_allowed(&rules[6],DOWN,1,13);
    wfc_tile_proto_add_allowed(&rules[6],LEFT,1,13);
    wfc_tile_proto_add_allowed(&rules[6],RIGHT,2,7,8);

    wfc_tile_proto_init(&rules[7],7,1);
    wfc_tile_proto_add_allowed(&rules[7],UP,1,4);
    wfc_tile_proto_add_allowed(&rules[7],DOWN,1,13);
    wfc_tile_proto_add_allowed(&rules[7],LEFT,2,6,7);
    wfc_tile_proto_add_allowed(&rules[7],RIGHT,2,7,8);


    wfc_tile_proto_init(&rules[8],8,1);
    wfc_tile_proto_add_allowed(&rules[8],UP,2,2,5);
    wfc_tile_proto_add_allowed(&rules[8],DOWN,1,13);
    wfc_tile_proto_add_allowed(&rules[8],LEFT,2,6,7);
    wfc_tile_proto_add_allowed(&rules[8],RIGHT,1,13);

    wfc_tile_proto_init(&rules[9],9,1);
    wfc_tile_proto_add_allowed(&rules[9],UP,1,4);
    wfc_tile_proto_add_allowed(&rules[9],DOWN,2,5,8);
    wfc_tile_proto_add_allowed(&rules[9],LEFT,1,4);
    wfc_tile_proto_add_allowed(&rules[9],RIGHT,2,7,8);

    wfc_tile_proto_init(&rules[10],10,1);
    wfc_tile_proto_add_allowed(&rules[10],UP,1,4);
    wfc_tile_proto_add_allowed(&rules[10],DOWN,2,3,8);
    wfc_tile_proto_add_allowed(&rules[10],LEFT,2,1,6);
    wfc_tile_proto_add_allowed(&rules[10],RIGHT,1,4);

    wfc_tile_proto_init(&rules[11],11,1);
    wfc_tile_proto_add_allowed(&rules[11],UP,2,0,5);
    wfc_tile_proto_add_allowed(&rules[11],DOWN,1,4);
    wfc_tile_proto_add_allowed(&rules[11],LEFT,1,4);
    wfc_tile_proto_add_allowed(&rules[11],RIGHT,2,1,8);

    wfc_tile_proto_init(&rules[12],12,1);
    wfc_tile_proto_add_allowed(&rules[12],UP,2,2.3);
    wfc_tile_proto_add_allowed(&rules[12],DOWN,1,4);
    wfc_tile_proto_add_allowed(&rules[12],LEFT,2,7,6);
    wfc_tile_proto_add_allowed(&rules[12],RIGHT,1,4);


    

    wfc_tile_proto_init(&rules[13],13,1);
    wfc_tile_proto_add_allowed(&rules[13],UP,4,6,7,8,13);
    wfc_tile_proto_add_allowed(&rules[13],DOWN,4,0,1,2,13);
    wfc_tile_proto_add_allowed(&rules[13],LEFT,4,2,5,8,13);
    wfc_tile_proto_add_allowed(&rules[13],RIGHT,4,0,3,6,13);


    
    
    



    
    
    
    

    /*
    struct wfc_tile_proto rules[6];
    wfc_tile_proto_init(&rules[0],0,6);
    wfc_tile_proto_add_allowed(&rules[0],UP,3,2,3,4);
    wfc_tile_proto_add_allowed(&rules[0],DOWN,2,2,5);
    wfc_tile_proto_add_allowed(&rules[0],LEFT,3,1,3,5);
    wfc_tile_proto_add_allowed(&rules[0],RIGHT,2,1,4);
    
    wfc_tile_proto_init(&rules[1],1,6);
    wfc_tile_proto_add_allowed(&rules[1],UP,3,2,3,4);
    wfc_tile_proto_add_allowed(&rules[1],DOWN,2,3,5);
    wfc_tile_proto_add_allowed(&rules[1],LEFT,2,0,4);
    wfc_tile_proto_add_allowed(&rules[1],RIGHT,3,0,2,5);
    
    wfc_tile_proto_init(&rules[2],2,6);
    wfc_tile_proto_add_allowed(&rules[2],UP,2,0,5);
    wfc_tile_proto_add_allowed(&rules[2],DOWN,3,0,1,4);
    wfc_tile_proto_add_allowed(&rules[2],LEFT,3,1,3,5);
    wfc_tile_proto_add_allowed(&rules[2],RIGHT,3,1,3,4);
    
    wfc_tile_proto_init(&rules[3],3,6);
    wfc_tile_proto_add_allowed(&rules[3],UP,3,0,1,5);
    wfc_tile_proto_add_allowed(&rules[3],DOWN,3,0,1,4);
    wfc_tile_proto_add_allowed(&rules[3],LEFT,3,0,2,4);
    wfc_tile_proto_add_allowed(&rules[3],RIGHT,3,0,2,5);

    wfc_tile_proto_init(&rules[4],4,6);
    wfc_tile_proto_add_allowed(&rules[4],UP,3,2,3,4);
    wfc_tile_proto_add_allowed(&rules[4],DOWN,3,0,1,4);
    wfc_tile_proto_add_allowed(&rules[4],LEFT,3,0,2,4);
    wfc_tile_proto_add_allowed(&rules[4],RIGHT,3,1,3,4);

    wfc_tile_proto_init(&rules[5],5,6);
    wfc_tile_proto_add_allowed(&rules[5],UP,3,0,1,5);
    wfc_tile_proto_add_allowed(&rules[5],DOWN,3,2,3,5);
    wfc_tile_proto_add_allowed(&rules[5],LEFT,3,1,3,5);
    wfc_tile_proto_add_allowed(&rules[5],RIGHT,3,0,2,5);
    */
    
    /*
    struct wfc_tile_proto rules[10];
    wfc_tile_proto_init(&rules[0],0,1);
    wfc_tile_proto_add_allowed(&rules[0],LEFT,2,0,1);
    wfc_tile_proto_add_allowed(&rules[0],RIGHT,2,0,2);
    wfc_tile_proto_add_allowed(&rules[0],UP,2,0,6);
    wfc_tile_proto_add_allowed(&rules[0],DOWN,2,0,5);

    wfc_tile_proto_init(&rules[1],1,1);
    wfc_tile_proto_add_allowed(&rules[1],LEFT,1,9);
    wfc_tile_proto_add_allowed(&rules[1],RIGHT,2,0,2);
    wfc_tile_proto_add_allowed(&rules[1],UP,2,1,3);
    wfc_tile_proto_add_allowed(&rules[1],DOWN,2,1,7);

    wfc_tile_proto_init(&rules[2],2,1);
    wfc_tile_proto_add_allowed(&rules[2],LEFT,2,0,1);
    wfc_tile_proto_add_allowed(&rules[2],RIGHT,1,9);
    wfc_tile_proto_add_allowed(&rules[2],UP,2,2,4);
    wfc_tile_proto_add_allowed(&rules[2],DOWN,2,2,8);

    wfc_tile_proto_init(&rules[3],3,1);
    wfc_tile_proto_add_allowed(&rules[3],LEFT,1,9);
    wfc_tile_proto_add_allowed(&rules[3],RIGHT,1,6);
    wfc_tile_proto_add_allowed(&rules[3],UP,1,9);
    wfc_tile_proto_add_allowed(&rules[3],DOWN,1,1);
    
    wfc_tile_proto_init(&rules[4],4,1);
    wfc_tile_proto_add_allowed(&rules[4],LEFT,1,6);
    wfc_tile_proto_add_allowed(&rules[4],RIGHT,1,9);
    wfc_tile_proto_add_allowed(&rules[4],UP,1,9);
    wfc_tile_proto_add_allowed(&rules[4],DOWN,1,2);

    wfc_tile_proto_init(&rules[5],5,1);
    wfc_tile_proto_add_allowed(&rules[5],LEFT,2,5,7);
    wfc_tile_proto_add_allowed(&rules[5],RIGHT,2,5,8);
    wfc_tile_proto_add_allowed(&rules[5],UP,2,0,6);
    wfc_tile_proto_add_allowed(&rules[5],DOWN,1,9);

    wfc_tile_proto_init(&rules[6],6,1);
    wfc_tile_proto_add_allowed(&rules[6],LEFT,2,3,6);
    wfc_tile_proto_add_allowed(&rules[6],RIGHT,2,4,6);
    wfc_tile_proto_add_allowed(&rules[6],UP,1,9);
    wfc_tile_proto_add_allowed(&rules[6],DOWN,2,0,5);

    wfc_tile_proto_init(&rules[7],7,1);
    wfc_tile_proto_add_allowed(&rules[7],LEFT,4,1,9);
    wfc_tile_proto_add_allowed(&rules[7],RIGHT,1,5);
    wfc_tile_proto_add_allowed(&rules[7],UP,1,1);
    wfc_tile_proto_add_allowed(&rules[7],DOWN,4,1,9);

    wfc_tile_proto_init(&rules[8],8,1);
    wfc_tile_proto_add_allowed(&rules[8],LEFT,1,5);
    wfc_tile_proto_add_allowed(&rules[8],RIGHT,1,9);
    wfc_tile_proto_add_allowed(&rules[8],UP,1,2);
    wfc_tile_proto_add_allowed(&rules[8],DOWN,1,9);

    wfc_tile_proto_init(&rules[9],9,255);
    wfc_tile_proto_add_allowed(&rules[9],LEFT,4,2,4,8,9);
    wfc_tile_proto_add_allowed(&rules[9],RIGHT,4,1,3,7,9);
    wfc_tile_proto_add_allowed(&rules[9],UP,4,5,7,8,9);
    wfc_tile_proto_add_allowed(&rules[9],DOWN,4,3,4,6,9);
    */

    
    wfc_state_init(&state, &grid, rules, num_tiles);

    /*
    for(int i=0;i<grid.width;i++)
    {
	struct wfc_cell *cell;
	
	cell = &grid.cells[i][0];
	wfc_cell_fix(cell,9);
	cell->fixed = false;

	cell = &grid.cells[i][grid.width-1];
	wfc_cell_fix(cell,9);
	cell->fixed = false;
	
	cell = &grid.cells[0][i];
	wfc_cell_fix(cell,9);
	cell->fixed = false;
	
	cell = &grid.cells[grid.height-1][i];
	wfc_cell_fix(cell,9);
	cell->fixed = false;
	
    }
    */
    

    //print_entropy(&grid);
    //printf("Wave function collapse: %u\n",wave_function_collapse(&grid,rules,4));
    
    
    wave_function_collapse(&state);
    wfc_trail_destroy(state.trail);
    free(state.trail);
    wfc_grid_destroy(state.grid);









	
    
	
	
    
	
	
    
    
    return 0;
}

