#include "bitvector.h"
#include <stdio.h>
#include <stdlib.h>

void bitvector_create(struct bitvector *bitvector)
{
    
    //set bits 0-num_set-1
    //make sure we clear out so we start at nothing.
    bitvector->bits=0;
    bitvector->num_set=0;
    
}
void bitvector_create_explicit(struct bitvector *bitvector, uint64_t bits);

inline bool bitvector_isset(struct bitvector *bitvector, uint8_t idx)
{
    return (bitvector->bits & ((uint64_t)1 << idx));
}
inline void bitvector_set(struct bitvector *bitvector, uint8_t idx)
{
    if(!bitvector_isset(bitvector,idx))
    {
	bitvector->bits |= ((uint64_t)1 << idx);
	bitvector->num_set++;
    }
}

void bitvector_set_all(struct bitvector *bitvector, uint8_t idx)
{
    bitvector->bits = (((uint64_t)2 <<(idx-1))-1);
    bitvector->num_set=idx;
    
}


inline void bitvector_unset(struct bitvector *bitvector, uint8_t idx)
{
    if(bitvector_isset(bitvector, idx))
    {
	bitvector->bits &= ~((uint64_t)1 << idx);
	bitvector->num_set--;
    }
}

uint8_t bitvector_max(struct bitvector *bitvector)
{
    uint64_t num= bitvector->bits;
 
    uint8_t msb = 0;
    num/=2;
    while (num != 0) {
        num/= 2;
        msb++;
    }
    
    return msb;
}

uint8_t bitvector_min(struct bitvector *bitvector)
{

    
    uint64_t num= bitvector->bits;
    
    uint8_t lsb = 0;
    while (!(num & 1))
    {
	//printf("next:\n");
	num >>= 1;
	lsb++;
    }
    return lsb;
    
}

void bitvector_fix(struct bitvector *bitvector,uint8_t idx)
{
    bitvector->bits = ((uint64_t)1 << idx);
    bitvector->num_set=1;

}


//This is slow improve if possible
int bitvector_next_set(struct bitvector *bitvector, uint8_t idx)
{
    uint8_t max=bitvector_max(bitvector);
    if(idx==max)
	return -1;
    
    for(uint8_t i=idx;i<=max;i++)
    {
	
	if(bitvector_isset(bitvector,i))
	{
	    return i;
	}
    }
    return -1;
}

uint8_t bitvector_count_set(struct bitvector *bitvector)
{
    uint8_t num_set = 0;
    uint8_t max=bitvector_max(bitvector);
    for(int i=0;i<max;i++)
    {
	if(bitvector_isset(bitvector,i))
	    num_set++;
    }

    return num_set;
}

struct bitvector bitvector_union(struct bitvector *bf0, struct bitvector *bf1)
{
    struct bitvector union_set;
    union_set.bits = bf0->bits | bf1->bits;
    union_set.num_set=bitvector_count_set(&union_set);
    
    return union_set;
}



uint8_t *bitvector_to_array(struct bitvector *bitvector)
{
    if(bitvector->num_set == 0)
	return NULL;
    
    uint8_t *array_set = malloc(bitvector->num_set*sizeof(uint8_t));
    uint8_t max = bitvector_max(bitvector);
    size_t idx=0;
    
    for(uint8_t i=0;i<=max;i++)
    {
	if(bitvector_isset(bitvector,i))    
	{
	    array_set[idx]=i;
	    idx++;
	}
	
    }

    return array_set;
}

