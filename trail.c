#include "trail.h"
#include <stdlib.h>

void wfc_mark_init(struct wfc_mark *w_m, uint8_t decision,struct wfc_cell *old)
{
    w_m->decision = decision;
    wfc_cell_copy(&w_m->fixed, old);
    w_m->propagated = NULL;
    w_m->next = NULL;
}
void wfc_mark_add_propagated(struct wfc_mark *w_m, struct wfc_cell *old)
{

    struct wfc_cell_node *node = w_m->propagated;
    while(node)
    {
	
	if(node->cell.x == old->x && node->cell.y == old->y)
	{
	    return;
	}
	node=node->next;
    }
    
    
    struct wfc_cell_node *old_node = malloc(sizeof(struct wfc_cell_node));
    wfc_cell_copy(&old_node->cell, old);


    if(w_m->propagated)
    {
	old_node->next = w_m->propagated;
	
    }
    else
    {
	old_node->next = NULL;
    }
    w_m->propagated = old_node;
    
}

void wfc_trail_init(struct wfc_trail *w_t)
{
    w_t->end = NULL;
    w_t->len = 0;
}

void wfc_trail_destroy(struct wfc_trail *w_t)
{
    struct wfc_mark *mark = w_t->end;
    while(mark)
    {
	struct wfc_mark *tmp_mark = mark;
	struct wfc_cell_node *node = tmp_mark->propagated;
	while(node)
	{
	    struct wfc_cell_node *tmp_node = node;
	    node = node->next;
	    free(tmp_node);
	}
	
	mark = mark->next;
	free(tmp_mark);
    }
}

void wfc_trail_add_mark(struct wfc_trail *w_t, struct wfc_mark *w_m)
{
    //assumes mark has beeen initialized beforehand.
    
    if(w_t->end)
    {
	w_m->next = w_t->end->next;
    }

    w_t->end=w_m;
    w_t->len++;
}

bool wfc_trail_backtrack(struct wfc_trail *w_t, struct wfc_grid *w_grid)
{
    //printf("Huh?\n");
    
    if(w_t->len == 0)
    {
	return false;
    }
    
    if(w_t->end)
    {
	
	//
	struct wfc_mark *mark = w_t->end;
	w_t->end = mark->next;
	
	wfc_cell_copy(&w_grid->cells[mark->fixed.x][mark->fixed.y],&mark->fixed);

	struct wfc_cell_node *node = mark->propagated;
	while(node)
	{
	    struct wfc_cell_node *tmp_node = node;
	    struct wfc_cell *to_restore = &node->cell;
	    wfc_cell_copy(&w_grid->cells[to_restore->x][to_restore->y],to_restore);
	    node = node->next;
	    free(tmp_node);
	}
	
	
	//remove the not viable solution for this cell
	bitset_remove(&w_grid->cells[mark->fixed.x][mark->fixed.y].opt,mark->decision);
	bool backtrack = false;
	if(wfc_cell_entropy(&w_grid->cells[mark->fixed.x][mark->fixed.y])==0)
	{
	    backtrack = true;
	}
	free(mark);
	w_t->len--;
	//if there are no viable solutions left backtrack again.
	if(backtrack)
	{
	    return wfc_trail_backtrack(w_t,w_grid);
	}
	return true;
    }
    return false;
}
