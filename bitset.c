#include "bitset.h"

void create_bitset(struct bitset *bitset)
{
    //bitset->id = id;
    bitvector_create(&bitset->domain);
}

void destroy_bitset(struct bitset *bitset)
{
    //nothing to do here for now.
}

void bitset_add(struct bitset *bitset, uint8_t value)
{
    bitvector_set(&bitset->domain, value);
}

void bitset_add_all(struct bitset *bitset, uint8_t value)
{
    bitvector_set_all(&bitset->domain, value);
}

inline void bitset_remove(struct bitset *bitset, uint8_t value)
{
    bitvector_unset(&bitset->domain,value);
}

inline bool bitset_contains(struct bitset *bitset, uint8_t value)
{
    bitvector_isset(&bitset->domain,value);
}

uint8_t bitset_max(struct bitset *bitset)
{
    return bitvector_max(&bitset->domain);
}

uint8_t bitset_min(struct bitset *bitset)
{
    return bitvector_min(&bitset->domain);
}

inline void bitset_fix(struct bitset *bitset, uint8_t value)
{
    return bitvector_fix(&bitset->domain,value);
}
inline uint8_t bitset_size(struct bitset *bitset)
{
    return bitset->domain.num_set;
}


int bitset_next(struct bitset *bitset, uint8_t value)
{
    return bitvector_next_set(&bitset->domain, value);
}

inline void bitset_restore(struct bitset *bitset, struct bitvector restore)
{
    bitset->domain=restore;
}

uint8_t *bitset_to_array(struct bitset *bitset)
{
    return bitvector_to_array(&bitset->domain);
}


struct bitset bitset_union(struct bitset *bs0, struct bitset *bs1)
{
    struct bitset union_set;
    union_set.domain=bitvector_union(&bs0->domain,&bs1->domain);

    return union_set;
}
