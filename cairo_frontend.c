#include <cairo.h>
#include <math.h>

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define LINE_WIDTH 1024

struct map
{
    uint16_t **tiles;
    int map_size;
};

void init_map_from_txt(struct map *map, char *filename)
{
    FILE *fp=fopen(filename,"r");
    char line[LINE_WIDTH];
    fgets(line,LINE_WIDTH,fp);
    map->map_size=atoi(line);
    printf("%s",line);

    map->tiles = malloc(map->map_size * sizeof(uint16_t *));

    for(int i=0;i<map->map_size;i++)
    {
	
	map->tiles[i] = malloc(map->map_size *sizeof(uint16_t));
	
    }
    
    for(int i=0;i<map->map_size;i++)
    {
	
	fgets(line,LINE_WIDTH,fp);
	char *tile = strtok(line," \n");
	printf("%s ",line);
	map->tiles[i][0] = atoi(tile);
	
	for(int j=1;j<map->map_size;j++)
	{
	    
	    tile = strtok(NULL," \n");
	    map->tiles[i][j] = atoi(tile);
	    printf("%s ",tile);
	    
	}
	
	printf("\n");
    }
    
}



int main(int argc, char *argv[])
{

    int tile_size = 32;
    int map_size = 9;

    struct map the_map;
    init_map_from_txt(&the_map,"the_map");
    
    
    cairo_surface_t *surface;
    cairo_t *cr;

    surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, the_map.map_size*tile_size,the_map.map_size*tile_size);
    cr = cairo_create (surface);

    
    cairo_surface_t *tiles[14];

    /*
    tiles[0]=cairo_image_surface_create_from_png("Tiles/0.png");
    tiles[1]=cairo_image_surface_create_from_png("Tiles/1.png");
    tiles[2]=cairo_image_surface_create_from_png("Tiles/2.png");
    tiles[3]=cairo_image_surface_create_from_png("Tiles/3.png");
    tiles[4]=cairo_image_surface_create_from_png("Tiles/4.png");
    tiles[5]=cairo_image_surface_create_from_png("Tiles/5.png");
    */
    
    /*
    tiles[0]=cairo_image_surface_create_from_png("Tiles/pool/0.png");
    tiles[1]=cairo_image_surface_create_from_png("Tiles/pool/1.png");
    tiles[2]=cairo_image_surface_create_from_png("Tiles/pool/2.png");
    tiles[3]=cairo_image_surface_create_from_png("Tiles/pool/3.png");
    tiles[4]=cairo_image_surface_create_from_png("Tiles/pool/4.png");
    tiles[5]=cairo_image_surface_create_from_png("Tiles/pool/5.png");
    tiles[6]=cairo_image_surface_create_from_png("Tiles/pool/6.png");
    tiles[7]=cairo_image_surface_create_from_png("Tiles/pool/7.png");
    tiles[8]=cairo_image_surface_create_from_png("Tiles/pool/8.png");
    tiles[9]=cairo_image_surface_create_from_png("Tiles/pool/9.png");
    */

    tiles[0]=cairo_image_surface_create_from_png("Tiles/terrain/0.png");
    tiles[1]=cairo_image_surface_create_from_png("Tiles/terrain/1.png");
    tiles[2]=cairo_image_surface_create_from_png("Tiles/terrain/2.png");
    tiles[3]=cairo_image_surface_create_from_png("Tiles/terrain/3.png");
    tiles[4]=cairo_image_surface_create_from_png("Tiles/terrain/4.png");
    tiles[5]=cairo_image_surface_create_from_png("Tiles/terrain/5.png");
    tiles[6]=cairo_image_surface_create_from_png("Tiles/terrain/6.png");
    tiles[7]=cairo_image_surface_create_from_png("Tiles/terrain/7.png");
    tiles[8]=cairo_image_surface_create_from_png("Tiles/terrain/8.png");
    tiles[9]=cairo_image_surface_create_from_png("Tiles/terrain/9.png");
    tiles[10]=cairo_image_surface_create_from_png("Tiles/terrain/10.png");
    tiles[11]=cairo_image_surface_create_from_png("Tiles/terrain/11.png");
    tiles[12]=cairo_image_surface_create_from_png("Tiles/terrain/12.png");
    tiles[13]=cairo_image_surface_create_from_png("Tiles/terrain/22.png");

    
    
    
    //tiles[6]=cairo_image_surface_create_from_png("Tiles/6.png");


    for(int i=0;i<the_map.map_size;i++)
    {
	
	for(int j=0;j<the_map.map_size;j++)
	{
	    
	    cairo_set_source_surface(cr, tiles[the_map.tiles[j][i]], i*tile_size, j*tile_size);
	    cairo_paint(cr);
	    
	}
	
    }

    cairo_surface_write_to_png (surface, "wfc_map.png");
    cairo_destroy (cr);
    cairo_surface_destroy (surface);


}
