#include <stdint.h>
#include <stddef.h>

#include "wfc_grid.h"

#ifndef TRAIL_H
#define TRAIL_H


struct wfc_cell_node
{
    struct wfc_cell cell;
    struct wfc_cell_node *next;
};

struct wfc_mark
{
    //to remove during untrailing
    uint8_t decision;
    struct wfc_cell fixed;
    struct wfc_cell_node *propagated;
    
    struct wfc_mark *next;
};

void wfc_mark_init(struct wfc_mark *w_m, uint8_t decision, struct wfc_cell *old);
void wfc_mark_add_propagated(struct wfc_mark *w_m, struct wfc_cell *old);


struct wfc_trail
{
    size_t len;
    struct wfc_mark *end;
};

void wfc_trail_init(struct wfc_trail *w_t);
void wfc_trail_destroy(struct wfc_trail *w_t);
void wfc_trail_add_mark(struct wfc_trail *w_t, struct wfc_mark *w_m);
bool wfc_trail_backtrack(struct wfc_trail *w_t, struct wfc_grid *w_grid);

#endif
